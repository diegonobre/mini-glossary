<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Glossary;
use AppBundle\Entity\Term;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Glossary controller.
 *
 * @Route("glossary")
 */
class GlossaryController extends Controller
{
    /**
     * Lists all glossary entities.
     *
     * @Route("/", name="glossary_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $glossaries = $em->getRepository('AppBundle:Glossary')->findAll();

        return $this->render('glossary/index.html.twig', array(
            'glossaries' => $glossaries,
        ));
    }

    /**
     * Creates a new glossary entity.
     *
     * @Route("/new", name="glossary_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $glossary = new Glossary();

        // start form with minimal terms
        $term1 = new Term();
        $term1->setName('');
        $glossary->addTerm($term1);

        $term2 = new Term();
        $term2->setName('');
        $glossary->addTerm($term2);

        $term3 = new Term();
        $term3->setName('');
        $glossary->addTerm($term3);

        $form = $this->createForm('AppBundle\Form\GlossaryType', $glossary);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // set logged in user
            $glossary->setUser($this->getUser());

            foreach ($glossary->getTerms() as $term) {
                $term->setGlossary($glossary);
                $term->setUser($this->getUser());
            }

            $em->persist($glossary);
            $em->flush();

            return $this->redirectToRoute('glossary_show', array('id' => $glossary->getId()));
        }

        return $this->render('glossary/new.html.twig', array(
            'glossary' => $glossary,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a glossary entity.
     *
     * @Route("/{id}", name="glossary_show")
     * @Method("GET")
     */
    public function showAction(Glossary $glossary)
    {
        $deleteForm = $this->createDeleteForm($glossary);

        return $this->render('glossary/show.html.twig', array(
            'glossary' => $glossary,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing glossary entity.
     *
     * @Route("/{id}/edit", name="glossary_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Glossary $glossary)
    {
        $deleteForm = $this->createDeleteForm($glossary);
        $editForm = $this->createForm('AppBundle\Form\GlossaryType', $glossary);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            foreach ($glossary->getTerms() as $term) {
                $term->setGlossary($glossary);
                $term->setUser($this->getUser());
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('glossary_edit', array('id' => $glossary->getId()));
        }

        return $this->render('glossary/edit.html.twig', array(
            'glossary' => $glossary,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a glossary entity.
     *
     * @Route("/{id}", name="glossary_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Glossary $glossary)
    {
        $form = $this->createDeleteForm($glossary);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // cascade remove terms and translations
            foreach ($glossary->getTerms() as $term) {
                $em->remove($term);

                foreach ($term->getTranslations() as $translation) {
                    $em->remove($translation);
                }
            }

            $em->remove($glossary);
            $em->flush();
        }

        return $this->redirectToRoute('glossary_index');
    }

    /**
     * Creates a form to delete a glossary entity.
     *
     * @param Glossary $glossary The glossary entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Glossary $glossary)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('glossary_delete', array('id' => $glossary->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
