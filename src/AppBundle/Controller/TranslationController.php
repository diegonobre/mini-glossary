<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Term;
use AppBundle\Entity\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Translation controller.
 *
 * @Route("translation")
 */
class TranslationController extends Controller
{
    /**
     * Lists all translation entities.
     *
     * @Route("/", name="translation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $translations = $em->getRepository('AppBundle:Translation')->findAll();

        return $this->render('translation/index.html.twig', array(
            'translations' => $translations,
        ));
    }

    /**
     * Creates a new translation entity.
     *
     * @Route("/new", name="translation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $translation = new Translation();
        $form = $this->createForm('AppBundle\Form\TranslationType', $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // set logged in user
            $translation->setUser($this->getUser());
            $em->persist($translation);
            $em->flush();

            return $this->redirectToRoute('translation_show', array('id' => $translation->getId()));
        }

        return $this->render('translation/new.html.twig', array(
            'translation' => $translation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new translation entity.
     *
     * @Route("/new/term/{termId}", name="translation_new_by_term")
     * @Method({"GET", "POST"})
     */
    public function newByTermAction(Request $request, $termId)
    {
        $translation = new Translation();

        $em = $this->getDoctrine()->getManager();
        $term = $em->getRepository('AppBundle:Term')->find($termId);

        $translation->setTerm($term);

        $form = $this->createForm('AppBundle\Form\TranslationType', $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // set logged in user
            $translation->setUser($this->getUser());
            $em->persist($translation);
            $em->flush();

            return $this->redirectToRoute('translation_show', array('id' => $translation->getId()));
        }

        return $this->render('translation/new.html.twig', array(
            'translation' => $translation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a translation entity.
     *
     * @Route("/{id}", name="translation_show")
     * @Method("GET")
     */
    public function showAction(Translation $translation)
    {
        $deleteForm = $this->createDeleteForm($translation);

        return $this->render('translation/show.html.twig', array(
            'translation' => $translation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing translation entity.
     *
     * @Route("/{id}/edit", name="translation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Translation $translation)
    {
        $deleteForm = $this->createDeleteForm($translation);
        $editForm = $this->createForm('AppBundle\Form\TranslationType', $translation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('translation_edit', array('id' => $translation->getId()));
        }

        return $this->render('translation/edit.html.twig', array(
            'translation' => $translation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a translation entity.
     *
     * @Route("/{id}", name="translation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Translation $translation)
    {
        $form = $this->createDeleteForm($translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($translation);
            $em->flush();
        }

        return $this->redirectToRoute('translation_index');
    }

    /**
     * Creates a form to delete a translation entity.
     *
     * @param Translation $translation The translation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Translation $translation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('translation_delete', array('id' => $translation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
