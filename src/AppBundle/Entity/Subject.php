<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Subject
 *
 * @ORM\Table(name="s_subject")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubjectRepository")
 */
class Subject
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Glossary", mappedBy="subject")
     */
    private $glossaries;

    public function __construct()
    {
        $this->glossaries = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Subject
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add glossary
     *
     * @param \AppBundle\Entity\Glossary $glossary
     *
     * @return Subject
     */
    public function addGlossary(\AppBundle\Entity\Glossary $glossary)
    {
        $this->glossaries[] = $glossary;

        return $this;
    }

    /**
     * Remove glossary
     *
     * @param \AppBundle\Entity\Glossary $glossary
     */
    public function removeGlossary(\AppBundle\Entity\Glossary $glossary)
    {
        $this->glossaries->removeElement($glossary);
    }

    /**
     * Get glossaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGlossaries()
    {
        return $this->glossaries;
    }

    public function __toString() {
        return $this->name;
    }
}
